import { Controller, Get, Param, Post, Body, Patch, Query, Delete } from "@nestjs/common";
import { UserService } from "../application/user.service";

@Controller('users')
export class UserController {
  constructor(private readonly userService: UserService) { }  

  @Get()
  public getUsers(@Query('search') search?: string): any {
    return this.userService.getListOfUsers(search);
  }

  @Post()
  public createUser(@Body() body: any): any {
    return this.userService.createUser(body);
  }

  @Patch(':id')
  public updateUser(@Body() body: any, @Param('id') userId: number): any {
    return this.userService.updateUser(userId, body);
  }
  
  @Delete(':id')
  public deleteUser(@Param('id') userId: number): any {
    return this.userService.deleteUser(userId);
  }
}