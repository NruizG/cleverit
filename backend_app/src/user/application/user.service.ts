import { HttpService, Injectable } from "@nestjs/common";
import { User } from "../domain/user.model";

@Injectable()
export class UserService {
  private uri = 'https://arsene.azurewebsites.net/UsersFullStack';
  private searchFilter = '?q='

  constructor(private http: HttpService) { }
  
  public async getListOfUsers(search: string): Promise<string[]> {
    return new Promise<string[]>((resolve, reject) => {
      const getUri = search
        ? `${this.uri}${this.searchFilter}${search}`
        : `${this.uri}`;
      console.log(getUri)
      this.http.get(getUri).subscribe(response => {
        resolve(response.data)
      }, error => {
        reject(error)
      })
    })
  }

  public async createUser(userParams: any): Promise<User> {
    return new Promise<User>((resolve, reject) => {
      const user = new User(userParams);
      this.http.post(this.uri, user).subscribe((response: any) => {
        user.id = response.data.id
        resolve(user);
      }, error => {
        reject(error);
      })
    })
  }

  public async updateUser(id: number, userParams: any): Promise<User> {
    return new Promise<User>((resolve, reject) => {
      const user = new User(userParams);
      this.http.patch(`${this.uri}/${id}`, userParams).subscribe((response: any) => {
        user.id = response.data.id;
        resolve(user);
      }, error => {
        reject(error);
      })
    })
  }

  public async deleteUser(userId: number): Promise<any> {
    return new Promise<any>((resolve, reject) => {
      this.http.delete(`${this.uri}/${userId}`).subscribe((response: any) => {
        resolve({
          statusCode: response.status,
          message: response.statusText
        });
      }, error => {
        reject(error);
      })
    })
  }
}