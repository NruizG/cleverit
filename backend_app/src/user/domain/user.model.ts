export class User {
  public id: number;
  public name: string;
  public lastname: string;
  public email: string;

  constructor(data: any = null) {
    if (data) {
      this.id = data.id
      this.name = data.name
      this.lastname = data.lastname
      this.email = data.email
    }
  }
}