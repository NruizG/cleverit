# Test Desarrollador Full-stack Typescript | CleverIT
## Descripción 📃

El proyecto fue desarrollado con la siguientes tecnologías para cada parte de
la arquitectura de la aplicación:

* Frontend:  Reactjs + Typescript + Redux
* Backend:  NestJS + Trypescript

### Dependencias

* NodeJS con yarn o npm
* Docker
* Docker-compose

## Despliegue con Docker 🐳 

Ambas aplicaciones estan dockerizadas y orquestadas por docker-compose. Para correr ambas instancias solo basta con ejecutar el siguiente comando y las aplicaciones estarán disponibles en los pueros 3000(frontend) 4000 (backend)

```bash
docker-compose up -d
```

## Despliegue 💾

Para el correcto despliegue de la aplicación se necesita NodeJS + algún gestor
de packetes como npm o yarn.

Primero entrar a ./backend_app y correr los siguientes comando correspondientes
a la instalación de dependencias y despliegue respectivamente



```bash
yarn #(or) npm install
````

```bash
yarn start:dev #(or) npm run start:dev
````

Despues ejecutar una tarea similar para ./frontend_app:

```bash
yarn  #(or) npm install
````

```bash
yarn start #(or) npm run start
````

Despues de ejecutar estas tareas exitosamente ambas aplicaciones estarán
disponibles en las siguientes URLs:

```curl
## Frontend
http://localhost:3000
````
```curl
## Backend
http://localhost:4000
````

* **Nota**: en localhost:4000/api pueden encontrar la apidoc correspondiente al backend.

😎😎😎  racias y que lo disfruten
