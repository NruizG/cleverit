import { TOGGLE_EDITOR, UPDATE_USERS, SELECT_USER } from '../actions/user.actions'

export const initialState = {
  userList: null,
  selectedUser: null,
  showEditor: false
};
  

// Generate the reducer with the actions
export const rootReducer = (state: any = initialState, action: any) => {
  switch (action.type) {
    case TOGGLE_EDITOR:
      return { ...state, showEditor: !state.showEditor}
    case UPDATE_USERS:
      return Object.assign({}, state, { ...action.payload });
    case SELECT_USER:
      return Object.assign({}, state, { ...action.payload });
    default:
      return state;
  }
}

export default rootReducer;
