import axios from 'axios';
import { Config } from '../config/config';
import { User } from '../types/user';

export default class UserService {
  public static searchQuery = '?search=';
  public static prefix = 'users';

  public static fetchUsers(search?: string): Promise<any> {
    return new Promise<any>((resolve, reject) => {
      const uri = search
        ? `${Config.API}/${this.prefix}${this.searchQuery}${search}`
        : `${Config.API}/${this.prefix}`;
      axios.get(uri)
        .then((response: any) => {
          resolve(response.data.map((value: any) => new User(value)));
        });
    });
  }

  public static updateUser(user: User): Promise<any> {
    return new Promise<any>((resolve, reject) => {
      axios.patch(`${Config.API}/${this.prefix}/${user.id}`, user)
        .then((response: any) => {
          resolve();
        });
    });
  }

  public static deleteUser(user: User): Promise<any> {
    return new Promise<any>((resolve, reject) => {
      axios.delete(`${Config.API}/${this.prefix}/${user.id}`)
        .then((response: any) => {
          resolve();
        });
    })
  }

  public static createUser(user: User): Promise<any> {
    return new Promise<any>((resolve, reject) => {
      axios.post(`${Config.API}/${this.prefix}`, user)
        .then((response: any) => {
          resolve(new User(response.data));
        });
    })
  }
}