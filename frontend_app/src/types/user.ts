export class User {
  public id: number | null  = null;
  public name: string | null = null;
  public lastname: string | null  = null;
  public email: string | null  = null;

  constructor(data: any = null) {
    if (data) {
      this.id = data.id || undefined;
      this.name = data.name || null;
      this.lastname = data.lastname || null;
      this.email = data.email || null;
    }
  }

  public update(data: any): any {
    this.id = data.id ? data.id : this.id;
    this.name = data.name ? data.name : this.name;
    this.lastname = data.lastname ? data.lastname : this.lastname;
    this.email = data.email ? data.email : this.email;
  }
}