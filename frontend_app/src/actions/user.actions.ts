import UserService from "../services/users.service";
import { User } from "../types/user";

export const UPDATE_USERS = "UPDATE_USERS";
export const TOGGLE_EDITOR = "TOGGLE_EDITOR";
export const SELECT_USER = 'SELECT_USER';

export const toggleEditor = () => ({
  type: TOGGLE_EDITOR
})

export const createUser = (user: User) => {
  return (dispatch: any, getState: any) => {
    UserService.createUser(user).then(response => {
      UserService.fetchUsers().then(users => {
        dispatch({
          type: UPDATE_USERS,
          payload: {
            userList: users,
            selectedUser: null,
            showEditor: false
          }
        });
      });
    })
  }
}

export const getUsers = (name?: string) => {
  return (dispatch: any, getState: any) => {
    UserService.fetchUsers(name).then(users => {
      dispatch({
        type: UPDATE_USERS,
        payload: {
          userList: users,
          selectedUser: null,
          showEditor: false
        }
      });
    })
  }
}

export const removeUser = (user: User) => {
  return (dispatch: any, getState: any) => {
    UserService.deleteUser(user).then(users => {
      UserService.fetchUsers().then(users => {
        dispatch({
          type: UPDATE_USERS,
          payload: {
            userList: users,
            selectedUser: null,
            showEditor: false
          }
        });
      })
    })
  }
}

export const updateUser = (user: User): any => {
  return (dispatch: any, getState: any) => {
    UserService.updateUser(user).then(users => {
      UserService.fetchUsers().then(users => {
        dispatch({
          type: UPDATE_USERS,
          payload: {
            userList: users,
            selectedUser: null,
            showEditor: false
          }
        });
      })
    })
  }
}

export const selectUser = (user: User | null): any => {
  return (dispatch: any, getState: any) => {
    dispatch({
      type: SELECT_USER,
      payload: {
        selectedUser: user
      }
    })
  }
}