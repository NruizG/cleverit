import React, { Component } from 'react';
import { Layout } from 'antd';
import { FileTextFilled } from '@ant-design/icons';
import './header.scss';

export default class Header extends Component {
  public render(): any {
    return (
      <Layout.Header>
        <FileTextFilled /> <span className="title">Clever Test</span>
      </Layout.Header>
    );
  }
}
