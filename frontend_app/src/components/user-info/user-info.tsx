import React, { Component } from 'react'
import './user-info.scss'
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { updateUser, selectUser, toggleEditor } from '../../actions/user.actions';
import { User } from '../../types/user';
import { EditOutlined } from '@ant-design/icons';
import { Button } from 'antd';
import { IUserInfo } from './user-info.interface';

class UserInfo extends Component<IUserInfo> {
  public renderUser = (user: User | null) => {
    if (!user) {
      return (
        <div className="no-content">Selecciona un usuario</div>
      )
    } else {
      return (
        <div>
          <div>"id": "{user.id}"</div>
          <div>"name": "{user.name}"</div>
          <div>"lastname": "{user.lastname}"</div>
          <div>"email": "{user.email}"</div>
        </div>
      )
    }
  }

  public renderEditButton = (user: User | null): any => {
    if (user) {
      return (
        <div>
          <Button type="primary" icon={<EditOutlined />} size="small"
            onClick={this.editUser.bind(this, user)}>
            Edit
          </Button>
          <Button type="primary" icon={<EditOutlined />} size="small" danger
            onClick={this.clearUser.bind(this)} className="clear">
            Clear
          </Button>
        </div>
      )
    }
  }

  public editUser = (user: User): void => {
    this.props.toggleEditor();

  }

  public clearUser = (): void => {
    this.props.selectUser(null);
  }


  public render(): any {
    return (  
      <div className="content__user-info">
        <div className="content__user-info__title">
          User's info
          {this.renderEditButton(this.props.user || null)}
        </div>
        <div className="content__user-info__content">
          {this.renderUser(this.props.user || null)}
        </div>
      </div>
    );
  }
}

const mapStateToProps = (state: any): any => ({
  user: state.selectedUser
})

const mapDispatchToProps = (dispatch: any): any => {
  return bindActionCreators({
    updateUser,
    selectUser,
    toggleEditor
  }, dispatch);
}

export default connect(mapStateToProps, mapDispatchToProps)(UserInfo)