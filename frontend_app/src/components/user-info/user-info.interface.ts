import { User } from "../../types/user";

export interface IUserInfo {
  updateUser?: any,
  user?: User,
  selectUser?: any,
  toggleEditor?: any
}