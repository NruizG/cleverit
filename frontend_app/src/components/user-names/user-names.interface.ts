import { User } from "../../types/user";

export interface IUserNames {
  users: User[],
  removeUser: any,
  selectUser: any
}