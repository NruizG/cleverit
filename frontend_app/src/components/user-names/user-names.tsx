import React, { Component } from 'react';
import './user-names.scss'
import { connect } from 'react-redux';
import { User } from '../../types/user';
import { Spin, Button, Popconfirm } from 'antd';
import { bindActionCreators } from 'redux';
import { removeUser, selectUser } from '../../actions/user.actions';
import { IUserNames } from './user-names.interface';

class UserName extends Component <IUserNames> {

  public selectUser = (user: User): void => {
    this.props.selectUser(user);
  }

  public listUsers = (users: User[]) => {
    if (users) {
      return users.map((user, key) => {
        return (
          <div className="content__user-names__list__item"
            onClick={this.selectUser.bind(this, user)}
            key={`item-container-${key}`}>
            {user.name} {user.lastname}
            <Popconfirm
              placement="topRight"
              title="Are you sure delete this user?"
              onConfirm={this.props.removeUser.bind(this, user)}
              okText="Yes"
              cancelText="No"
            >
              <Button type="primary" shape="circle" danger size="small">
                X
              </Button>
            </Popconfirm>
            
          </div>
        )
      })
    } else {
      return (
      <div className="loading-container">
        <Spin />
      </div>
      );
    }
  }


  public render() {
    return (
      <div className="content__user-names">
        <div className="content__user-names__title">
          User names
        </div>
        <div className="content__user-names__list">
          {this.listUsers(this.props.users)}
        </div>
      </div>
    );
  }
}

const mapStateToProps = (state: any) => ({
  users: state.userList
})

const mapDispatchToProps = (dispatch: any) => {
  return bindActionCreators({
    removeUser,
    selectUser
  }, dispatch);
}

export default connect(mapStateToProps, mapDispatchToProps)(UserName)