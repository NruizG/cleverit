import React, { Component } from 'react';
import { Button, Input, message } from 'antd';
import { SearchOutlined } from '@ant-design/icons';
import { UserAddOutlined } from '@ant-design/icons';
import './search-bar.scss';
import { connect } from 'react-redux';
import { toggleEditor, getUsers } from '../../actions/user.actions';
import { bindActionCreators } from 'redux';
import { ISearchBar } from './search-bar.interface';

export class SearchBar extends Component<ISearchBar> {
  public state = {
    name: null
  }

  public setName = (options: any, event: any) => {
    if (event.target.value === '') {
      this.props.getUsers();
    } else {
      this.setState({
        name: event.target.value
      });
    }
  }

  public search = () => {
    if (this.state.name) {
      this.props.getUsers(this.state.name);
    } else {
      message.warning('Debe ingresar un nombre para poder buscar');
    }
  }

  public render(): any {
    return (
      <div className="content__inputs">
        <div className="content__inputs__search">
          <Input size="large" placeholder="Search user" className="input"
            onChange={this.setName.bind(this, {})}
            prefix={<SearchOutlined />} />
          <Button type="primary" size="large" onClick={this.search.bind(this)}>
            Search
          </Button>
        </div>
        <Button type="primary" size="large" icon={<UserAddOutlined />}
          onClick={this.props.toggleEditor} className="button">
          Add User
        </Button>
      </div>
    );
  }
}

const mapStateToProps = (state: any) => ({
  userList: state.userList
})

const mapDispatchToProps = (dispatch: any) => {
  return bindActionCreators({
    toggleEditor,
    getUsers
  }, dispatch)
}

export default connect(mapStateToProps, mapDispatchToProps)(SearchBar)
