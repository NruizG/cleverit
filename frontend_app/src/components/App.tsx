import React, { Component } from 'react';
import { Layout } from 'antd';
import './App.scss';
import Header from './header/header';
import SearchBar from './search-bar/search-bar';
import UserName from './user-names/user-names';
import UserInfo from './user-info/user-info';
import Editor from './editor/editor'
import { connect } from 'react-redux';
import { getUsers } from '../actions/user.actions';
import { bindActionCreators } from 'redux';
  
const { Content } = Layout;

class App extends Component<{getUsers: any}> {
  public componentDidMount = () => {
    this.props.getUsers();
  }

  render() {
    return (
      <Layout className="layout">
        <Header></Header>
        <Content className="content">
          <div className="content__title">
            Clever Test
          </div>
          <SearchBar></SearchBar>
          <UserName></UserName>
          <UserInfo></UserInfo>
        </Content>
        <Editor></Editor>
      </Layout>
    );
  }
}

const mapDispatchToProps = (dispatch: any) => {
  return bindActionCreators({
    getUsers
  }, dispatch);
}

export default connect(null, mapDispatchToProps)(App);
