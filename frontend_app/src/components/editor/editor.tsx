import React, { Component } from 'react';
import { Button, Modal } from 'antd';
import { Input } from 'antd';
import './editor.scss';
import { connect } from 'react-redux';
import { toggleEditor, createUser, updateUser } from '../../actions/user.actions';
import { User } from '../../types/user';
import { bindActionCreators } from 'redux';
import { IEditor } from './editor.interface';

class Editor extends Component<IEditor> {
  public state = {
    user: new User(),
    loading: false,
    toggleEditor: false
  };
  
  public setStateFunc = (user: User): void => {
    this.setState({
      user
    });
  }

  public setValue = (name: "name" | "lastname" | "email", event: any): void => {
    const user = this.state.user;
    user[name] = event.target.value;
    this.setStateFunc(user);
  }

  public showModal = (): any => {
    this.setState({
      visible: true,
    });
  };

  public handleOk = (): any => {
    this.setState({ loading: true });
    if (this.state.user.id) {
      this.props.updateUser(this.state.user);
    } else {
      this.props.createUser(this.state.user);
    }
    this.props.toggleEditor();
  };

  public static getDerivedStateFromProps(nextProps: any, prevState: any): any {
    if (nextProps.visible !== prevState.toggleEditor) {
      return {
        user: new User({
          id: nextProps.selectedUser ? nextProps.selectedUser.id : '',
          name: nextProps.selectedUser ? nextProps.selectedUser.name : '',
          lastname: nextProps.selectedUser ? nextProps.selectedUser.lastname : '',
          email: nextProps.selectedUser ? nextProps.selectedUser.email : '',
        }),
        toggleEditor: nextProps.visible
      };
    }
    return null;
  }

  public render(): any {
    const { loading } = this.state;
    return (
      <>
        <Modal
          className="modal"
          title="Add User"
          visible={this.props.visible}
          onOk={this.handleOk}
          onCancel={this.props.toggleEditor}
          key="modal"
          footer={[
            <div className="save-button" key="footerContainer">
              <Button key="submit" type="primary" loading={loading}
                onClick={this.handleOk} className="button">
                Save user
              </Button>
            </div>
          ]}
        >
          <div className="modal__field">
            <div className="modal__field__label">User name</div>
            <Input placeholder="User name" key="name"
              value={this.state.user.name || ''}
              onChange={this.setValue.bind(this, 'name')}/>
          </div>
          <div className="modal__field">
            <div className="modal__field__label">Last name</div>
            <Input placeholder="Last name" key="lastname"
              value={this.state.user.lastname || ''}
              onChange={this.setValue.bind(this, 'lastname')}/>
          </div>
          <div className="modal__field">
            <div className="modal__field__label">Email</div>
            <Input placeholder="Email" key="email"
              value={this.state.user.email || ''}
              onChange={this.setValue.bind(this, 'email')}/>
          </div>
        </Modal>
      </>
    );
  }
}

const mapStateToProps = (state: any) => ({
  visible: state.showEditor,
  selectedUser: state.selectedUser
})

const mapDispatchToProps = (dispatch: any) => {
  return bindActionCreators({
    toggleEditor,
    createUser,
    updateUser
  }, dispatch);
}

export default connect(mapStateToProps, mapDispatchToProps)(Editor)