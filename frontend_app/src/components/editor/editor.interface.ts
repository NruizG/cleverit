import { User } from "../../types/user";

export interface IEditor {
  visible: boolean,
  toggleEditor: any,
  selectedUser: User,
  createUser: any,
  updateUser: any
}